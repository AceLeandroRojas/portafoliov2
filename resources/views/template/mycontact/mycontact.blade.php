<!-- start sectoion contact -->
<div id="contact" class="paddsection">
  <div class="container">
    <div class="contact-block1">
      <div class="row">

        <div class="col-lg-6">
          <div class="contact-contact">

            <h2 class="mb-30">CONTACTO</h2>

            <ul class="contact-details">
              <li><span>CLL 100, Chico</span></li>
              <li><span>Bogotá, Colombia</span></li>
              <li><span>+57 123 45 67</span></li>
              <li><span>jleandrorojas@unipanamericana.edu.co</span></li>
            </ul>

          </div>
        </div>

        <div class="col-lg-6">
          <form action="" method="post" role="form" class="contactForm">
            <div class="row">

              <div id="sendmessage">Tu mensaje ha sido enviado. Gracias!</div>
              <div id="errormessage"></div>

              <div class="col-lg-6">
                <div class="form-group contact-block1">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Correo" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="12" data-rule="required" data-msg="Please write something for us" placeholder="Mensaje"></textarea>
                  <div class="validation"></div>
                </div>
              </div>

              <div class="col-lg-12">
                <input type="submit" class="btn btn-defeault btn-send" value="Enviar mensaje">
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- start sectoion contact -->


<!-- start section footer -->
<div id="footer" class="text-center">
  <div class="container">
    <div class="socials-media text-center">

      <ul class="list-unstyled">
        <li><a href="#"><i class="ion-social-facebook"></i></a></li>
        <li><a href="#"><i class="ion-social-twitter"></i></a></li>
        <li><a href="#"><i class="ion-social-instagram"></i></a></li>
        <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
        <li><a href="#"><i class="ion-social-tumblr"></i></a></li>
        <li><a href="#"><i class="ion-social-dribbble"></i></a></li>
      </ul>

    </div>

    <p>&copy; Copyrights Folio. Todos los derechos reservados.</p>

    <div class="credits">
      <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Folio
      -->
      Diseñado por <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>

  </div>
</div>
<!-- End section footer -->