<!-- start section services -->
  <div id="services">
    <div class="container">

        <div class="services-carousel owl-theme">

          <div class="services-block">

            <i class="ion-ios-browsers-outline"></i>
            <span>DISEÑO WEB</span>
            <p class="separator">El diseño web es una actividad que consiste en la planificación, diseño e implementación de sitios web. </p>

          </div>

          <div class="services-block">

            <i class="ion-ios-lightbulb-outline"></i>
            <span>PROTOTIPOS</span>
            <p class="separator">Son fotomontajes que permiten a los diseñadores gráficos y web mostrar al cliente cómo quedaran sus diseños. </p>

          </div>

          <div class="services-block">

            <i class="ion-ios-color-wand-outline"></i>
            <span>DESARROLLO WEB</span>
            <p class="separator">Creación de sitios web para Internet o una intranet. Para conseguirlo se hace uso de tecnologías de software del lado del servidor y del cliente. </p>

          </div>

          <div class="services-block">

            <i class="ion-social-android-outline"></i>
            <span>APPS MÓVILES</span>
            <p class="separator">Es una aplicación informática diseñada para ser ejecutada en teléfonos inteligentes, tabletas y otros dispositivos móviles. </p>

          </div>

          <div class="services-block">

            <i class="ion-ios-analytics-outline"></i>
            <span>ANALÍTICA DE DATOS</span>
            <p class="separator">La analítica de negocios incluye el proceso de examinar conjuntos de datos para extraer conclusiones sobre la información que contienen. </p>

          </div>

          <div class="services-block">

            <i class="ion-ios-camera-outline"></i>
            <span>FRONTEND</span>
            <p class="separator">En diseño de software el front-end es la parte del software que interactúa con los usuarios. </p>

          </div>

        </div>

    </div>

  </div>
  <!-- end section services -->