<!-- start section navbar -->
<nav id="main-nav">
  <div class="row">
    <div class="container">

      <div class="logo">
        <a href="#"><img src="images/logo.png" alt="logo"></a>
      </div>

      <div class="responsive"><i data-icon="m" class="ion-navicon-round"></i></div>

      <ul class="nav-menu list-unstyled">
        <li><a href="#header" class="smoothScroll">Inicio</a></li>
        <li><a href="#about" class="smoothScroll">Acerca de mi</a></li>
        <li><a href="#portfolio" class="smoothScroll">Portafolio</a></li>
        <li><a href="#contact" class="smoothScroll">Contacto</a></li>
      </ul>

    </div>
  </div>
</nav>
<!-- End section navbar -->