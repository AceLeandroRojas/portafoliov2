<!-- start section about us -->
<div id="about" class="paddsection">
  <div class="container">
    <div class="row justify-content-between">

      <div class="col-lg-4 ">
        <div class="div-img-bg">
          <div class="about-img">
            <img src="images/meLR.jpg" class="img-responsive" alt="me">
          </div>
        </div>
      </div>

      <div class="col-lg-7">
        <div class="about-descr">

          <p class="p-heading">Capaz de construir, diseñar e implementar sistemas de información en los diversos SO incluyendo actividades de soporte técnico de modo integral y personalizado.</p>
          <p class="separator">Me caracteriza  ejercer mi labor creativa, ética y con responsabilidad. Con experiencia en administración y gestión del área de atención al cliente enfocado al cumplimiento de metas.</p>

        </div>

      </div>
    </div>
  </div>
</div>
<!-- end section about us -->