<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PruebaController extends Controller {
	public function prueba($param){
		return 'Estoy en mi controlador de prueba recibiendo este parametro '. $param;
	}
}